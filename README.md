
# Model Selection Visualizer

The goal of model selection is to find a model that provides the best fit to a set of data, without overfitting the data. Different criterion for assessing the degree of overfitting abound; typically they make reference to the number of parameters a model includes. Too few parameters, and your model will not be flexible enough to fit the data. Too many, and your model will be too flexible and end up overfitting the data.

This program calculates and plots different measures of model quality as a function of the number of parameters in the model. The models used are all polynomial fits, and the k-th model is the set of all (k-1)-order polynomials. There are three measures of model quality used in this program: Log Likelihood, AIC, and BIC. A gentle introduction to each of these criterion can be found [on my blog](https://risingentropy.com/how-to-learn-from-data-part-2-evaluating-models/).

Below are some sample runs, with different choices of the underlying function generating the data. In the top graph, the x-axis is the number of parameters in the model (i.e. the order of the polynomial), and the y-axis measures the degree of fit (smaller numbers are better scores). "Error stdev" in the title stands for the standard deviation of the noise added to the ground truth.

## Ground Truth: f(x) = cos(x)

![](Media/cos.png)

## Ground Truth: f(x) = 1/x

![](Media/inv.png)

## Ground Truth: f(x) = cosh(x/3)

![](Media/cosh.png)

## Ground Truth: f(x) = 100cos(x/3)

![](Media/100cos.png)


