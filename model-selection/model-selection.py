import matplotlib.pyplot as plt
import numpy as np

#range of x-values
lo = -10
hi = 10
sigma = .5 #uncertainty in dataset
N = 100 #number of data points

kRange = range(1, 26) #the different amounts of parameters to test
criterion = "LogL" #which criterion to use, choose from AIC, BIC, and LogL
function = "cos(x)" #description of function, purely aesthetic

#ground truth: the function that generates the data
def f(x):
    return np.cos(x)

''' Don't change anything below this line '''

def y(coeff, x): 
    terms = [coeff[i] * x**i for i in range(len(coeff))]
    return sum(terms)

def generateRandomData(lo, hi, sigma, N):
    X = np.linspace(lo, hi, N)
    E = np.random.normal(0, sigma, N)
    Y = [f(X[i]) + E[i] for i in range(N)]
    return X, Y

def printPolynomial(coeff):
    print(*[str(int(10**3*coeff[i])/10**3) +
        "*x^" + str(i) + " + " for i in range(k)])

def bestPolynomialFit(k, X, Y): # best kth order polynomial fit
    A = [sum([x**n for x in X]) for n in range(2*k + 1)]
    M = np.array( [[A[n + m] for n in range(k)] for m in range(k)] )
    B = [sum([Y[i]*X[i]**n for i in range(len(X))]) for n in range(k)]
    coeff = np.linalg.inv(M).dot(B)
    return coeff

def SOS(coeff, X, Y): #calculate sum of squares
    terms = [(Y[i] - y(coeff, X[i]))**2 for i in range(len(X))]
    return sum(terms)

def getScores(kRange, X, Y, sigma):
    logLScores, aicScores, bicScores = [], [], []
    
    for k in kRange:
        coeff = bestPolynomialFit(k, X, Y)
        sos = SOS(coeff, X, Y)
        logL = -N/2*np.log(2*np.pi*sigma**2) - sos/(2*sigma**2)
        aic = k - logL
        bic = k/2*np.log(N) - logL

        logLScores.append(-logL)
        aicScores.append(aic)
        bicScores.append(bic)
        
    return logLScores, aicScores, bicScores

#generate N data points from ground truth function, with appropriate noise
X, Y = generateRandomData(lo, hi, sigma, N)
logL, aic, bic = getScores(kRange, X, Y, sigma)

kAIC, kBIC, kLogL = np.argmin(aic), np.argmin(bic), np.argmin(logL)
coeffAIC = bestPolynomialFit(kAIC, X, Y)
coeffBIC = bestPolynomialFit(kBIC, X, Y)
coeffLogL = bestPolynomialFit(kLogL, X, Y)

# show scores as a function of parameter number
plt.subplot(2,1,1)
plt.plot(kRange, logL, "-r", label = "Log Likelihood")
plt.plot(kLogL+kRange[0], logL[kLogL], ".r")
plt.plot(kRange, aic, "-g", label = "AIC")
plt.plot(kAIC+kRange[0], aic[kAIC], ".g")
plt.plot(kRange, bic, "-b", label = "BIC")
plt.plot(kBIC+kRange[0], bic[kBIC], ".b")
plt.title("Ground Truth: f(x) = " + function + "\nError stdev: " + str(sigma))
plt.legend()
plt.grid(which="both")

#plot best curve according to AIC
plt.subplot(234)
plt.plot(X, Y, '.k')
Xdense = np.linspace(lo, hi, 20*N)
plt.plot(Xdense, f(Xdense), '--g', label = "True function")
plt.plot(Xdense, y(coeffAIC, Xdense), '-g',
    label = "AIC: Order " + str(len(coeffAIC)-1) + " Polynomial")
plt.legend()

#plot best curve according to BIC
plt.subplot(235)
plt.plot(X, Y, '.k')
Xdense = np.linspace(lo, hi, 20*N)
plt.plot(Xdense, f(Xdense), '--b', label = "True function")
plt.plot(Xdense, y(coeffBIC, Xdense), '-b',
    label = "BIC: Order " + str(len(coeffBIC)-1) + " Polynomial")
plt.legend()

#plot best curve according to BIC
plt.subplot(236)
plt.plot(X, Y, '.k')
Xdense = np.linspace(lo, hi, 20*N)
plt.plot(Xdense, f(Xdense), '--r', label = "True function")
plt.plot(Xdense, y(coeffLogL, Xdense), '-r',
    label = "LogL: Order " + str(len(coeffLogL)-1) + " Polynomial")
plt.legend()

plt.show()
